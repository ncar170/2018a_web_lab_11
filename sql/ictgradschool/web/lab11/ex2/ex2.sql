-- Answers to exercise 2 questions

SELECT st.fname, st.lname FROM unidb_students AS st, unidb_attend AS at
WHERE st.id = at.id AND at.dept = 'comp' AND at.num = '219';

SELECT st.fname, st.lname FROM unidb_students AS st, unidb_courses AS cs
WHERE st.id = cs.rep_id AND NOT country = 'NZ';

SELECT lect.office FROM unidb_lecturers AS lect, unidb_teach AS tc
WHERE lect.staff_no = tc.staff_no AND  tc.num = '219';

SELECT DISTINCT st.fname, st.lname FROM unidb_students AS st, unidb_teach AS tc, unidb_attend AS at
WHERE  st.id =at.id AND tc.staff_no = '707' AND at.dept = tc.dept AND at.num = tc.num;
-- Relate the student's ID in the st table to the student's ID in the at table; then isolate the courses taught by the staff_no; then compare the dept and num from both tables.

SELECT st.fname, st.lname, mt.fname, mt.lname FROM unidb_students AS st
INNER JOIN unidb_students AS mt ON st.id = mt.mentor;

SELECT fname, lname FROM unidb_lecturers AS lect
WHERE lect.office LIKE 'G%'
UNION
SELECT fname, lname FROM unidb_students AS st
WHERE NOT st.country = 'NZ';

SELECT fname, lname FROM unidb_students AS st, unidb_courses AS cs
WHERE st.id = cs.rep_id AND cs.dept = 'comp' AND cs.num = '219'
UNION
  SELECT fname, lname FROM unidb_lecturers AS lect, unidb_courses AS cs
WHERE lect.staff_no = cs.coord_no AND cs.dept = 'comp' AND cs.num = '219';
