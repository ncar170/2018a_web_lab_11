-- Answers to exercise 1 questions

SELECT DISTINCT dept FROM unidb_courses;

SELECT DISTINCT semester FROM unidb_attend;

SELECT DISTINCT dept, num FROM unidb_attend;

SELECT fname, lname, country FROM unidb_students
ORDER BY fname;

SELECT fname, lname, mentor FROM unidb_students
ORDER BY mentor;

SELECT fname, lname, office FROM unidb_lecturers
ORDER BY office;

SELECT fname,lname FROM unidb_lecturers
WHERE staff_no > 500;

SELECT fname, lname FROM unidb_students
WHERE id BETWEEN 1668 AND 1824;

SELECT fname, lname, country FROM unidb_students
WHERE country = 'NZ' OR country = 'US' OR country = 'AU';

SELECT fname, lname FROM unidb_lecturers
WHERE office LIKE 'G%';

SELECT dept, num FROM unidb_courses
WHERE NOT dept = 'comp';

SELECT fname, lname, country FROM unidb_students
WHERE country = 'MX' OR  country = 'FR';