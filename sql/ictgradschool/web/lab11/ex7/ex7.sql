-- Answers to exercise 1 questions

DROP TABLE IF EXISTS db_league;
DROP TABLE IF EXISTS db_teams;
DROP TABLE IF EXISTS db_players;

CREATE TABLE db_league (
  team_id INT (2) NOT NULL,
  team_name VARCHAR(60) NOT NULL,
  PRIMARY KEY (team_id)
);

CREATE TABLE db_teams (
  team_name VARCHAR(60) NOT NULL,
  team_number INT (2) NOT NULL,
  city VARCHAR(30) NOT NULL,
  points INT NOT NULL,
  captain VARCHAR(60),
  PRIMARY KEY (team_name),
  FOREIGN KEY (team_number) REFERENCES db_league (team_id)
);

CREATE TABLE db_players (
  player_number INT NOT NULL,
  fname VARCHAR(30),
  lname VARCHAR(30),
  age INT,
  nationality VARCHAR(30),
  team_name VARCHAR(60),
  PRIMARY KEY (player_number),
  FOREIGN KEY (team_name) REFERENCES db_teams (team_name)
);

CREATE TABLE db_plays_for (
  player_number INT NOT NULL,
  team_name VARCHAR(60) NOT NULL,
  PRIMARY KEY (player_number, team_name),
  FOREIGN KEY (player_number) REFERENCES db_players (player_number),
  FOREIGN KEY (team_name) REFERENCES  db_teams (team_name)
);

INSERT INTO db_league (team_id, team_name) VALUES
  (01, 'Auckland City'),
  (02, 'Wellington City'),
  (03, 'Canterbury United'),
  (04, 'Otago United'),
  (05, 'Taranaki Rovers'),
  (06, 'Hamilton City'),
  (07, 'Tasman United');

INSERT INTO db_teams (team_name, team_number, city, points, captain) VALUES
  ('Auckland City', 01, 'Auckland', 35, 'Frank Lampard'),
  ('Wellington City', 02, 'Wellington', 28, 'Wayne Rooney'),
  ('Canterbury United', 03, 'Christchurch', 37, 'Lionel Messi'),
  ('Otago United', 04, 'Dunedin', 32, 'Ronaldo'),
  ('Taranaki Rovers', 05, 'New Plymouth', 24, 'Gary Neville'),
  ('Hamilton City', 06, 'Hamilton', 22, 'Stephen Donald'),
  ('Tasman United', 07, 'Nelson', 31, 'Tim Perry');

INSERT INTO db_players (player_number, fname, lname, age, nationality, team_name) VALUES
  (01, 'Eden', 'Hazard', 27, 'BE', 'Auckland City'),
  (02, 'Dan', 'Carter', 33, 'NZ', 'Canterbury United'),
  (03, 'Mark', 'Gerrard', 38, 'UK', 'Wellington City'),
  (04, 'Ronnie', 'Davis', 34, 'AU', 'Hamilton City'),
  (05, 'Freddie', 'MacDonald', 19, 'NZ', 'Canterbury United'),
  (06, 'Matty', 'Taylor', 23, 'US', 'Tasman United'),
  (07, 'Frank', 'Lampard', 39, 'UK', 'Auckland City'),
  (08, 'Wayne', 'Rooney', 34, 'UK', 'Wellington City'),
  (09, 'Lionel', 'Messi', 32, 'AR', 'Canterbury United'),
  (10, 'Ronaldo', NULL, 33, 'PR', 'Otago United'),
  (11, 'Gary', 'Neville', 38, 'UK', 'Taranaki Rovers'),
  (12, 'Stephen', 'Donald', 34, 'NZ', 'Hamilton City'),
  (13, 'Tim', 'Perry', 30, 'NZ', 'Tasman United');

SELECT fname, lname FROM db_players
WHERE team_name = 'Canterbury United';

SELECT team_name, points FROM db_teams
ORDER BY points DESC;

INSERT INTO db_plays_for (player_number, team_name) VALUES
  (01, 'Auckland City'),
  (02, 'Canterbury United'),
  (03, 'Wellington City'),
  (04, 'Hamilton City'),
  (05, 'Canterbury United'),
  (06, 'Tasman United'),
  (07, 'Auckland City'),
  (08, 'Wellington City'),
  (09, 'Canterbury United'),
  (10, 'Otago United'),
  (11, 'Taranaki Rovers'),
  (12, 'Hamilton City'),
  (13, 'Tasman United');

SELECT fname, lname FROM db_players
WHERE age BETWEEN 28 AND 33;

SELECT captain FROM db_teams
WHERE team_name = 'Auckland City' OR team_name = 'Canterbury United';

SELECT fname, lname FROM db_players
WHERE NOT nationality = 'NZ';

SELECT pl.fname, pl.lname FROM db_players AS pl, db_teams AS tm
WHERE pl.nationality = 'NZ';

SELECT COUNT(player_number), nationality
FROM db_players
GROUP BY nationality
HAVING COUNT(player_number) > 2;

SELECT db_players.lname, db_teams.city
FROM db_players
RIGHT JOIN db_teams
  ON db_players.team_name = db_teams.team_name
ORDER BY db_players.lname;

SELECT db_players.lname, db_players.age, db_players.nationality, db_teams.team_name
FROM db_players
RIGHT JOIN db_teams
  ON db_players.team_name = db_teams.team_name
ORDER BY db_players.age;

SELECT db_players.lname, db_teams.city
FROM db_players
LEFT JOIN db_teams
  ON db_players.team_name = db_teams.team_name
ORDER BY db_players.lname;

SELECT A.lname AS LastName1, B.lname AS LastName2, A.nationality
FROM db_players A, db_players B
WHERE A.player_number <> B.player_number
AND A.nationality = B.nationality
ORDER BY A.nationality;